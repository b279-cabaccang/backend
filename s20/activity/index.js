// console.log("Hello World");

// console.log("Hello World");

//Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here

for(let i = 0; i < string.length; i++){
	if(string[i] == "s" ||
		string[i] == "p" ||
		string[i] == "r" ||
		string[i] == "c" ||
		string[i] == "l" ||
		string[i] == "f" ||
		string[i] == "r" ||
		string[i] == "g" ||
		string[i] == "t" ||
		string[i] == "x" ||
		string[i] == "d"){
		filteredString = filteredString + string[i];
		continue;
	}
	
}
console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null

    }
} catch(err){

}