// console.log("Hello wrld");

// GET method, retrieve all titles
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(title => {
	const titleMap = title.map(dataToMap => dataToMap.title)
		console.log(titleMap);
});
/*
.then(title => {
	const titleMap = title.map(dataToMap => {
		return {
			title: dataToMap.title
		};
	});
	console.log(titleMap);
});

fetch retrieve a single to do list item
*/

// GET method, retrieve a single to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

// GET method, title and status of the to do list item
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(titleAndStatus => {
	const mapped = titleAndStatus.map(dataToMap => {
		return {
			title: dataToMap.title,
			completed: dataToMap.completed
		};
	});
		console.log(mapped);
});

// POST method, create a to do list item
fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		userId: 1,
		id: 201,
		title: "zed",
		completed: true
	})
})
.then(res => res.json())
.then(json => console.log(json))

// PUT method, update a to do list item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		description: "wdadwa",
		userId: 599,
		status: "Complete",
		title: "PUT METHOD!",
		dateCompleted: "May 22, 2023"
	})
})
.then(res => res.json())
.then(json => console.log(json))

// PATCH method, update a to do list item
fetch("https://jsonplaceholder.typicode.com/todos/2", {
	method: "PUT",
	headers: {"Content-Type": "application/json"},
	body: JSON.stringify({
		userId: 1,
		title: "PATCH METHOD!",
		status: "Complete",
		dateCompleted: "May 23, 2023"
	})
})
.then(res => res.json())
.then(json => console.log(json))

// DELETE method
fetch("https://jsonplaceholder.typicode.com/todos/3", {
	method: "DELETE"
});