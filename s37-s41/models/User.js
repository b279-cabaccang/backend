// Activity s37

const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "FIRST NAME IS REQUIRED!"]
	},
	lastName: {
		type: String,
		required: [true, "LAST NAME IS REQUIRED!"]
	},
	email: {
		type: String,
		required: [true, "EMAIL IS REQUIRED!"]
	},
	password: {
		type: String,
		required: [true, "PASSWORD IS REQUIRED!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "MOBILE NUMBER IS REQUIRED!"]
	},
	enrollments: [
			{
				courseId: {
					type: String,
					required: [true, "COURSE ID IS REQUIRED!"],
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: "Enrolled"
				}
			}
		]
});



module.exports = mongoose.model("User", userSchema);