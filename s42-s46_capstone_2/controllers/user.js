const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Product = require("../models/Product.js");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo,
		fullAddress: reqBody.fullAddress
		
	})
	
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else {
			return "new user registered";
		}
	})
};

// User authentication (User login)
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return "user doesn't exist"
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			}
			else {
				return "wrong password"
			}
		}
	})


}

// Non-admin user checkout (create order)
module.exports.purchase = async(userId, productId, reqBody) => {
	let user = await User.findById(userId.id);
	let product = await Product.findById(productId);
	const currentDate = new Date();


	const subtotal = reqBody.quantity * product.price;
	const newOrder = {
		products: [
			{
				productId: product._id,
				productName: product.name,
				description: product.description,
				quantity: reqBody.quantity,
				price: product.price
			}

		],
		purchasedOn: currentDate,
		totalAmount: subtotal
	}

	user.orderedProduct.push(newOrder);
	product.userOrders.push({
		userId: userId.id,
		totalAmount: subtotal, 
		purchasedOn: currentDate
	});

	let isUserUpdated = await user.save().then(isUserUpdated => isUserUpdated)
	let isProductUpdated = await product.save().then(isProductUpdated => isProductUpdated)

	if (isUserUpdated && isProductUpdated) {
        return "Purchase successful";
    } else {
        return "Something went wrong with your request. Please try again later.";
    }
}


// Retrieve User Details
module.exports.getProfile = (userId) => {
	return User.findById(userId)
	.select("-orderedProduct")
	.then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}
		else{
			result.password = "";
			return result;
		}

	})

}

// Retrieve all users
module.exports.getAllProfiles = () => {
	return User.find({}).then(result => {
		return result;
	})
}

// STRETCH GOAL1: Set user as admin (ONLY ADMINS CAN DO THIS)
module.exports.setUserAdmin = (reqParams, reqBody, isAdmin) => {
	if(isAdmin){
			let userToAdmin = {
			isAdmin: true
		}

		return User.findByIdAndUpdate(reqParams.id, userToAdmin).then((course, error) => {
			if(error){
				return false;
			}
			else{
				return "User converted to admin!";
			}
		})
	}
	
	let message = Promise.resolve("You don't have the access rights to do this action.");

	return message.then((value) => {
		return value
	})
	
}

// STRETCH GOAL2: Retrieve authenticated user's orders (ONLY ADMINS)
module.exports.getUserOrders = (userId) => {
  return User.findById(userId).then((result) => {
    const products = [];
    const orderedProducts = result.orderedProduct;

    for (let i = 0; i < orderedProducts.length; i++) {
      const product = orderedProducts[i].products[0];
      products.push(product);
    }
    
    return products;
  });
}


// STRETCH GOAL3: Retrieve all orders (Admin only)

// STRETCH GOAL4: Add to cart
module.exports.addToCart = async(userId, orders) => {
  let user = await User.findById(userId.id);
 	const currentDate = new Date();
  let products = [];
  let totalAmount = 0;

  for (let i = 0; i < orders.length; i++) {
    let product = await Product.findById(orders[i].productId);

    const newProduct = {
      productId: product._id,
      productName: product.name,
      price: product.price,
      quantity: orders[i].quantity
    }

    let subtotal = product.price * orders[i].quantity;

    user.orderedProduct.push({
    	products: [newProduct],
    	purchasedOn: currentDate,
    	price: product.price,
    	subtotal: subtotal,
    	quantity: orders[i].quantity
    })

    totalAmount = totalAmount + subtotal;
    products.push(newProduct);
    product.userOrders.push({
    	userId: userId.id,
    	totalAmount: subtotal,
    	purchasedOn: currentDate
    });
    await product.save();
  }

  // Add totalAmount to the last order object in the orderedProduct array
  user.orderedProduct[user.orderedProduct.length - 1].totalAmount = totalAmount;

  let isUserUpdated = await user.save();

  if (isUserUpdated) {
    return "Purchase successful";
  } else {
    return "Something went wrong with your request. Please try again later.";
  }
}
