const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js");
const auth = require("../auth.js");

// User registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


// User authentication (User login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Non-admin user checkout (create order)
router.post("/checkout", auth.verify, (req, res) => {
	let userId = auth.decode(req.headers.authorization);
	let productId = req.body.productId;

	if(userId.isAdmin){
		res.send("Only customers are allowed to purchase.")
	}
	else{
		userController.purchase(userId, productId, req.body).then(resultFromController => res.send(resultFromController))
	}
	
})

// Retrieve User Details
router.get("/:id/userDetails", auth.verify, (req, res) => {
	const userId = auth.decode(req.headers.authorization);
	const searchUser = req.params.id;

	if(userId){
		userController.getProfile(searchUser).then(resultFromController => res.send(resultFromController))
	}
	else{
		res.send("You must be logged in to be able to search for user details.")
	}
	
});

// Retrieve All Users
router.get("/all", (req, res) => {
	userController.getAllProfiles().then(resultFromController => res.send(resultFromController));
})


// STRETCH GOAL1: Set user as admin (ONLY ADMINS CAN DO THIS)
router.put("/:id/setUserAdmin", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	userController.setUserAdmin(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})


// STRETCH GOAL2: Retrieve authenticated user's orders
router.get("/myOrders", auth.verify, (req, res) => {
		const userId = auth.decode(req.headers.authorization).id;
  userController.getUserOrders(userId).then((resultFromController) => res.send(resultFromController));
});


// STRETCH GOAL3: Retrieve all orders (Admin only)


// STRETCH GOAL4: Add to cart
router.post("/addToCart", auth.verify, async (req, res) => {
  const userId = auth.decode(req.headers.authorization);
  const orders = req.body.orders;

  if (userId.isAdmin) {
    res.send("Only customers are allowed to purchase.");
  } else {
      const result = await userController.addToCart(userId, orders);
        res.send(result);
 	}
 });

// Allows us to export the "router" object that will be access in our index.js file.
module.exports = router;