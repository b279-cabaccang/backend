const express = require("express");
const router = express.Router();
const productController = require("../controllers/product.js");
const auth = require("../auth.js");




// Create Product (Admin only)
router.post("/", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	
});

// Retrieve all products (Admin only)
router.get("/all", auth.verify, (req, res) => {
	const data = auth.decode(req.headers.authorization);
	productController.getAllProducts(data.isAdmin).then(resultFromController => res.send(resultFromController));
})

// Retrieve all active products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

// Retrieve single product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})

// Update product information
router.put("/:productId", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})

// Archive product (Admin only)
router.put("/:id/archive", auth.verify, (req, res) => {
	let isAdmin = auth.decode(req.headers.authorization).isAdmin
	productController.archiveProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
})











// Allows us to export the "router" object that will be accessed in our "index.js" file

module.exports = router;