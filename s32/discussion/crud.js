// client to server = JSON (POST/ PUT/PATCH)
// server to client = STRING (GET)

let http = require("http");

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}

];

http.createServer(function(req, res){
	// Route for returning all items upon receiving a GET request
	if(req.url == "/users" && req.method == "GET"){
		// Requests the "/users" path and GET information

	// Sets the status code to 200, denoting OK
	// Sets response output to JSON data type
		res.writeHead(200, {"Content-Type": "application/json"});
		// Input has to be data type STRING
		// This string input will be converted to desired output data type which has been set to JSON
		// This is done because requests and responses sent between client and a node JS server requires the information to be sent and received as a stringified JSON
		res.write(JSON.stringify(directory));
		res.end();

	}

	if(req.url == "/users" && req.method == "POST"){
		// Declare and intialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on
		let requestBody = '';

		// A stream is a sequence of data
		// Data is received from the client and is processed in the "data" stream
		// The information provided from the request object enters a sequence called "data" the code below will be triggered
		// data step - this reads the "data" stream and processes it as the request body
		req.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});
		req.on('end', function(){
			console.log(typeof reqBody);

			// Converts the string reqBody to JSON
			reqBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": reqBody.name,
				"email": reqBody.email
			}
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {"Content-Type": "application/json"});
			res.write(JSON.stringify(newUser));
			res.end();
		});
	}
}).listen(4001);

console.log("Server running at localhost: 4001");



